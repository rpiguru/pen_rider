#include <Arduino.h>

// Library
#include <ESP8266WiFi.h>
// WiFi settings
const char* ssid = "TP-LINK_REAL3D";
const char* password = "dkanehahffk";
// Time to sleep (in seconds):

const int sleepTimeS = 5;
// Host

void setup()
{
  // We cannot use GPIO16, BECAUSE it is WAKE pin which is used for deep sleep mode!!!!
  pinMode(13, OUTPUT);
  // Serial
  Serial.begin(9600);
  Serial.println("ESP8266 in normal mode");

  // Connect to WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
    delay(250);
    digitalWrite(13, HIGH);    // turn the LED off by making the voltage LOW
    delay(250);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  // Print the IP address
  Serial.println(WiFi.localIP());
  delay(1000);

  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);              // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);              // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);              // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);

  Serial.println();
  Serial.println("closing connection");
  // Sleep
  Serial.println("ESP8266 in sleep mode");
  ESP.deepSleep(sleepTimeS * 1000000);
}

void loop()
{
}
