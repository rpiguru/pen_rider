#include <Wire.h>
#include "algorithm.h"
#include "max30102.h"

#define INT_PIN 10
#define MAX_BRIGHTNESS 255

int debug = 0;

uint32_t aun_ir_buffer[100]; //infrared LED sensor data
uint32_t aun_red_buffer[100];  //red LED sensor data

int n_ir_buffer_length; //data length
int n_spo2;  //SPO2 value
int8_t ch_spo2_valid;  //indicator to show if the SPO2 calculation is valid
int n_heart_rate; //heart rate value
int8_t  ch_hr_valid;  //indicator to show if the heart rate calculation is valid
uint8_t uch_dummy;
uint8_t tmp;
uint32_t un_min, un_max, un_prev_data, un_brightness;  //variables to calculate the on-board LED brightness that reflects the heartbeats
int i;
float f_temp;
bool b_first = true;
    
void setup() {
  Serial.begin(9600);
//  pinMode(INT_PIN, INPUT);  //pin D10 connects to the interrupt output pin of the MAX30102
  Serial.println("Starting test code...");
  delay(5000);
  Wire.begin();
  Wire.setClock(I2C_BUS_SPEED);
  maxim_max30102_reset(); //resets the MAX30102
  maxim_max30102_read_reg(REG_INTR_STATUS_1, &uch_dummy);  //Reads/clears the interrupt status register
  maxim_max30102_init();  //initialize the MAX30102
  b_first = true;
}

void loop(){
  
//  for (i=0; i < 30; i++){
//    maxim_max30102_write_reg(i, i*2);
//    delay(100);
//    maxim_max30102_read_reg(i, &uch_dummy);
//    delay(100);
//  }
//  maxim_max30102_read_reg(0xFE, &uch_dummy);
//  maxim_max30102_read_reg(0xFF, &uch_dummy);
//  delay(1000);
//  return;
  un_min = 0x3FFFF;
  un_max = 0;
  n_ir_buffer_length = 100;  //buffer length of 100 stores 4 seconds of samples running at 25sps

//  Serial.println(digitalRead(INT_PIN));
  
  if (b_first){
    b_first = false;
    un_brightness = 0;     
  
//  read the first 100 samples, and determine the signal range
    for(i = 0; i < n_ir_buffer_length; i++){
//    while(digitalRead(INT_PIN) == 1){
//      delay(1);
//    }
      maxim_max30102_read_fifo((aun_red_buffer+i), (aun_ir_buffer+i));  //read from MAX30102 FIFO
      if(un_min > aun_red_buffer[i])
        un_min = aun_red_buffer[i];  //update signal min
      if(un_max < aun_red_buffer[i])
        un_max = aun_red_buffer[i];  //update signal max
//      Serial.print(F("red=")); Serial.print(aun_red_buffer[i], DEC);
//      Serial.print(F(", ir=")); Serial.println(aun_ir_buffer[i], DEC);
    }
    un_prev_data = aun_red_buffer[i];
    //calculate heart rate and SpO2 after first 100 samples (first 4 seconds of samples)
    maxim_heart_rate_and_oxygen_saturation(aun_ir_buffer, n_ir_buffer_length, aun_red_buffer, &n_spo2, &ch_spo2_valid, &n_heart_rate, &ch_hr_valid); 

    Serial.print(F("HR="));Serial.print(n_heart_rate, DEC);
    Serial.print(F(", HRvalid="));Serial.print(ch_hr_valid, DEC);
    Serial.print(F(", SPO2="));Serial.print(n_spo2, DEC);
    Serial.print(F(", SPO2Valid="));Serial.println(ch_spo2_valid, DEC);
  }
  else{
  //dumping the first 25 sets of samples in the memory and shift the last 75 sets of samples to the top
  for(i = 25; i < 100; i++){
      aun_red_buffer[i-25] = aun_red_buffer[i];
      aun_ir_buffer[i-25] = aun_ir_buffer[i];
  
      //update the signal min and max
      if(un_min > aun_red_buffer[i])
        un_min = aun_red_buffer[i];
      if(un_max < aun_red_buffer[i])
        un_max = aun_red_buffer[i];
    }
  
    //take 25 sets of samples before calculating the heart rate.
    for(i=75; i<100; i++)
    {
      un_prev_data = aun_red_buffer[i-1];
//        while(digitalRead(INT_PIN) == 1){
//          delay(1);
//        }
      maxim_max30102_read_fifo((aun_red_buffer+i), (aun_ir_buffer+i));
      //calculate the brightness of the LED
      if(aun_red_buffer[i] > un_prev_data)
      {
        f_temp = aun_red_buffer[i]-un_prev_data;
        f_temp /= (un_max-un_min);
        f_temp *= MAX_BRIGHTNESS;
        f_temp = un_brightness-f_temp;
        if(f_temp < 0)
          un_brightness = 0;
        else
          un_brightness = (int)f_temp;
      }
      else
      {
        f_temp = un_prev_data - aun_red_buffer[i];
        f_temp /= (un_max-un_min);
        f_temp *= MAX_BRIGHTNESS;
        un_brightness += (int)f_temp;
        if(un_brightness > MAX_BRIGHTNESS)
          un_brightness = MAX_BRIGHTNESS;
      }
    
      //send samples and calculation result to terminal program through UART
//      Serial.print(F("red=")); Serial.print(aun_red_buffer[i], DEC);
//      Serial.print(F(", ir=")); Serial.print(aun_ir_buffer[i], DEC);
//      Serial.println();
    }
    maxim_heart_rate_and_oxygen_saturation(aun_ir_buffer, n_ir_buffer_length, aun_red_buffer, &n_spo2, &ch_spo2_valid, &n_heart_rate, &ch_hr_valid); 
    Serial.print(F("HR=")); Serial.print(n_heart_rate, DEC);
    Serial.print(F(", HRvalid=")); Serial.print(ch_hr_valid, DEC);
    Serial.print(F(", SPO2=")); Serial.print(n_spo2, DEC);
    Serial.print(F(", SPO2Valid=")); Serial.println(ch_spo2_valid, DEC);
  }
  delay(1000);
}

bool maxim_max30102_write_reg(uint8_t uch_addr, uint8_t uch_data)
{
//  Serial.print("Writing register:: addr => ");Serial.print(uch_addr);Serial.print(", val => ");Serial.print(uch_data);
  Wire.beginTransmission(I2C_ADDR);
  Wire.write(uch_addr);
  Wire.write(uch_data);
  tmp = Wire.endTransmission();
//  Serial.print(", Result: ");Serial.println(tmp);
  return true;
}

bool maxim_max30102_read_reg(uint8_t uch_addr, uint8_t *puch_data)
{
//  Serial.print("Reading register :: Addr => ");Serial.print(uch_addr);Serial.print(", Read count => ");
  Wire.beginTransmission(I2C_ADDR);
  Wire.write(uch_addr);
  Wire.endTransmission(false);
  tmp = Wire.requestFrom(I2C_ADDR, 1);  // Returns the byte count of reading data
//  Serial.print(tmp);Serial.print(", Val => ");
  *puch_data = Wire.read();
//  Serial.println(*puch_data);
  return true;
}

bool maxim_max30102_init()
{
  if(!maxim_max30102_write_reg(REG_INTR_ENABLE_1,0xc0)) // INTR setting
    return false;
  if(!maxim_max30102_write_reg(REG_INTR_ENABLE_2,0x00))
    return false;
  if(!maxim_max30102_write_reg(REG_FIFO_WR_PTR,0x00))  //FIFO_WR_PTR[4:0]
    return false;
  if(!maxim_max30102_write_reg(REG_OVF_COUNTER,0x00))  //OVF_COUNTER[4:0]
    return false;
  if(!maxim_max30102_write_reg(REG_FIFO_RD_PTR,0x00))  //FIFO_RD_PTR[4:0]
    return false;
  if(!maxim_max30102_write_reg(REG_FIFO_CONFIG,0x4f))  //sample avg = 4, fifo rollover=false, fifo almost full = 17
    return false;
  if(!maxim_max30102_write_reg(REG_MODE_CONFIG,0x03))   //0x02 for Red only, 0x03 for SpO2 mode 0x07 multimode LED
    return false;
  if(!maxim_max30102_write_reg(REG_SPO2_CONFIG,0x27))  // SPO2_ADC range = 4096nA, SPO2 sample rate (100 Hz), LED pulseWidth (411uS)
    return false;
  
  if(!maxim_max30102_write_reg(REG_LED1_PA,0x24))   //Choose value for ~ 7mA for LED1
    return false;
  if(!maxim_max30102_write_reg(REG_LED2_PA,0x24))   // Choose value for ~ 7mA for LED2
    return false;
  if(!maxim_max30102_write_reg(REG_PILOT_PA,0x7f))   // Choose value for ~ 25mA for Pilot LED
    return false;
  return true;  
}

bool maxim_max30102_read_fifo(uint32_t *pun_red_led, uint32_t *pun_ir_led)
{
  uint8_t un_temp;
  uint8_t uch_temp;
  *pun_ir_led = 0;
  *pun_red_led = 0;
  maxim_max30102_read_reg(REG_INTR_STATUS_1, &uch_temp);
  maxim_max30102_read_reg(REG_INTR_STATUS_2, &uch_temp);
  Wire.beginTransmission(I2C_ADDR);
  Wire.write(REG_FIFO_DATA);
  Wire.endTransmission(false);
  Wire.requestFrom(I2C_ADDR, 6);  
  un_temp = Wire.read();
  un_temp <<= 16;
  *pun_red_led += un_temp;
  un_temp = Wire.read();
  un_temp <<= 8;
  *pun_red_led += un_temp;
  un_temp = Wire.read();
  *pun_red_led += un_temp;
  
  un_temp = Wire.read();
  un_temp <<= 16;
  *pun_ir_led += un_temp;
  un_temp = Wire.read();
  un_temp <<= 8;
  *pun_ir_led += un_temp;
  un_temp = Wire.read();
  *pun_ir_led += un_temp;
  *pun_red_led &= 0x03FFFF;  //Mask MSB [23:18]
  *pun_ir_led &= 0x03FFFF;  //Mask MSB [23:18]
  return true;
}

bool maxim_max30102_reset()
{
    if(!maxim_max30102_write_reg(REG_MODE_CONFIG, 0x40))
        return false;
    else
        return true;    
}


