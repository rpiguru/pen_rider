#include <SoftwareSerial.h>
#include <TinyGPS.h>
#include <Wire.h>
#include <Arduino.h>
#include "algorithm.h"
#include "max30102.h"
#include <ESP8266WiFi.h>
#include <SPI.h>
#include "SX1272.h"
#include <Ticker.h>
extern "C" {
#include "user_interface.h"
}

Ticker flipper;

//////////////////////////////////////// Change these values for each tag ////////////////////////////////////////

const char* uuid = "TAG_700000";    // Change UUID value to TAG_XXXXXX !
#define node_addr 70                // LoRa Address of this tag


#define PREPARE_SEC 10              // Preparing seconds before reading all values
#define sleepTimeS 10               // Sleep time in sec

//////////////////////////////////////// ADXL345 ////////////////////////////////////////
#define DEVICE (0x53) // Device address as specified in data sheet
#define ADXL345_MG2G_MULTIPLIER (0.004)
#define SENSORS_GRAVITY_STANDARD          (SENSORS_GRAVITY_EARTH)
#define SENSORS_GRAVITY_EARTH             (9.80665F)              /**< Earth's gravity in m/s^2 */

byte adxl_buff[6];
float adxl_value[3];
float adxl_cal[3] = {0, 0, 0};

char POWER_CTL = 0x2D;    //Power Control Register
char DATA_FORMAT = 0x31;
char DATAX0 = 0x32;    //X-Axis Data 0

//////////////////////////////////////// MLX96014 ////////////////////////////////////////
float temp_ambient = 0.0;
int cnt_temp_amb = 0;
float acc_temp_amb = 0;
float temp_object = 0.0;
int cnt_temp_obj = 0;
float acc_temp_obj = 0;

//////////////////////////////////////// GPS Module ////////////////////////////////////////

TinyGPS gps;
SoftwareSerial ss(0, 10);    // RX, TX
static void smartdelay(unsigned long ms);
float flat, flon;
unsigned long age;

char buf[100];
int count = 0;

/////////////////////////////////////// InAir9 LoRa ///////////////////////////
#define FCC_US_REGULATION
#define MAX_DBM 14
#define BAND868
const uint32_t DEFAULT_CHANNEL = CH_10_868;

// #define WITH_APPKEY
// #define WITH_ACK

// CHANGE HERE THE LORA MODE
#define LORAMODE  1
#define DEFAULT_DEST_ADDR 1

#ifdef WITH_APPKEY
uint8_t my_appKey[4]={5, 6, 7, 8};
#endif

#define PRINTLN                   Serial.println("")
#define PRINT_CSTSTR(fmt,param)   Serial.print(F(param))
#define PRINT_STR(fmt,param)      Serial.print(param)
#define PRINT_VALUE(fmt,param)    Serial.print(param)
#define FLUSHOUTPUT               Serial.flush();

#ifdef WITH_ACK
#define NB_RETRIES 2
#endif

double temp;
unsigned long nextTransmissionTime = 0L;
char float_str[20];
uint8_t message[100];
int loraMode = LORAMODE;

int EN_PWR = 2;        // Power Enable pin

///////////////////////////////////////////////  Analog Heart Rate Sensor   //////////////////////////////
int fadeRate = 0;
// these variables are volatile because they are used during the interrupt service routine!
volatile int BPM;                   // used to hold the pulse rate
volatile int Signal;                // holds the incoming raw data
volatile int IBI = 600;             // holds the time between beats, must be seeded!
volatile boolean Pulse = false;     // true when pulse wave is high, false when it's low
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.
int cnt_a_hr = 0;
int acc_a_hr = 0;

///////////////////////////////////////////////  Local Variables //////////////////////////////
long startSend;
long endSend;
uint8_t app_key_offset=0;
int e;
int a_hr = 0;
int hr_retry = 0;


void setup() {
//  ESP.wdtDisable();
//  ESP.wdtEnable(WDTO_8S);
  ESP.wdtFeed();    // VERY IMPORTANT!!!
//  delay(1000);
  WiFi.disconnect();
  WiFi.mode(WIFI_OFF);
  WiFi.forceSleepBegin();
  delay(1000);

  pinMode(EN_PWR, OUTPUT);         // Pin that enable/disable power of peripherals
  // Enable all peripherals
  digitalWrite(EN_PWR, LOW);

  Serial.begin(115200);
  Serial.print("\n\nUUID: ");  Serial.println(uuid);

  interruptSetup();                 // sets up to read Pulse Sensor signal every 2mS

  // Initialize LoRa Module
  // Power ON the module
  sx1272.ON();
  int e;
  // Set transmission mode and print the result
  e = sx1272.setMode(loraMode);
  Serial.print("Setting Mode: state "); Serial.println(e);
  // enable carrier sense
  sx1272._enableCarrierSense=true;
  // Select frequency channel
  e = sx1272.setChannel(DEFAULT_CHANNEL);
  Serial.print("Setting Channel: state "); Serial.println(e);
  // Set Power DBM
  e = sx1272.setPowerDBM((uint8_t)MAX_DBM);
  Serial.print("Setting Power: state "); Serial.println(e);
  // Set the node address and print the result
  e = sx1272.setNodeAddress(node_addr);
  Serial.print("Setting node addr: state "); Serial.println(e);
  Serial.println("SX1272 successfully configured.");

  // Initialize Accelormeter...w
  Wire.begin();
  Wire.setClock(I2C_BUS_SPEED);
  initialize_accel();
  delay(1000);

}

void loop() {
  ESP.wdtFeed();    // VERY IMPORTANT!!!

  // Read Accelerometer's values and display them.
  readAccel(false);
  Serial.print("-- Accelerometer's values ->  X: ");Serial.print(adxl_value[0]);
  Serial.print("  Y: ");Serial.print(adxl_value[1]);
  Serial.print("  Z: ");Serial.println(adxl_value[2]);

  delay(50);   // It is recommended to add delay between different I2C reads

  read_temperature_object();
  read_temperature_ambient();
  Serial.print("\tAmbient = "); Serial.print(temp_ambient); Serial.println(" *F");
  Serial.print("\tObject = "); Serial.print(temp_object); Serial.println(" *F");
  delay(50);

  print_gps_data();

  hr_retry = 0;
  // We will wait for 0.5 second to get valid heart rate value from analog heart rate sensor
  while (true){
    if (!QS)
      if (hr_retry < 10){
        smartdelay(10);
        hr_retry ++;
      }
      else{
        a_hr = 0;
        break;
      }
    else{
      a_hr = BPM;
      QS = false;
      break;
    }
  }

  if (count < PREPARE_SEC){
    Serial.println(PREPARE_SEC - count);
    count ++;
    if (temp_ambient >= 98 && temp_ambient <= 106){
      acc_temp_amb += temp_ambient;
      cnt_temp_amb ++;
    }
    if (temp_object >= 98 && temp_object <= 106){
      acc_temp_obj += temp_object;
      cnt_temp_obj ++;
    }
    if (a_hr >= 40 && a_hr <= 100){
      acc_a_hr += a_hr;
      cnt_a_hr ++;
    }
    smartdelay(1000);
  }
  else{
    if (cnt_temp_amb == 0)
      acc_temp_amb = 0;
    else
      acc_temp_amb /= cnt_temp_amb;
    if (cnt_temp_obj == 0)
      acc_temp_obj = 0;
    else
      acc_temp_obj /= cnt_temp_obj;
    if (cnt_a_hr == 0)
      acc_a_hr = 0;
    else
      acc_a_hr /= cnt_a_hr;
    cnt_temp_amb = 0;
    cnt_temp_obj = 0;
    cnt_a_hr = 0;
    // Publish sensor data
    sprintf(buf, "ID:%s,X:%d.%02d,Y:%d.%02d,Z:%d.%02d,TO:%d.%02d,TA:%d.%02d,H:%d,LA:%d.%04d,LO:%d.%04d",
//    sprintf(buf, "ID:%s,X:%d.%02d,Y:%d.%02d,Z:%d.%02d,TO:%d.%02d,TA:%d.%02d,H:%d",
        uuid,
        (int)adxl_value[0], abs((int)(adxl_value[0]*100)%100),
        (int)adxl_value[1], abs((int)(adxl_value[1]*100)%100),
        (int)adxl_value[2], abs((int)(adxl_value[2]*100)%100),
        (int)acc_temp_obj, abs((int)(acc_temp_obj*100)%100),
        (int)acc_temp_amb, abs((int)(acc_temp_amb*100)%100),
//        acc_a_hr);
        acc_a_hr,
        (int)flat, abs((int)(flat*1000000)%1000000),
        (int)flon, abs((int)(flon*1000000)%1000000));

//    Serial.println(buf);

    ESP.wdtFeed();    // VERY IMPORTANT!!!
    ESP.wdtDisable();

  // Send message through LoRa
#ifdef WITH_APPKEY
      app_key_offset = sizeof(my_appKey);
      // set the app key in the payload
      memcpy(message,my_appKey,app_key_offset);
#endif
      uint8_t r_size;
      // then use app_key_offset to skip the app key
      r_size=sprintf((char*)message+app_key_offset, "\\!#%s", buf);

      Serial.print("Sending "); Serial.println((char*)(message + app_key_offset));
      Serial.print("Real payload size is "); Serial.println(r_size);
      int pl = r_size + app_key_offset;
      sx1272.CarrierSense();
      startSend = millis();

#ifdef WITH_APPKEY
      // indicate that we have an appkey
      sx1272.setPacketType(PKT_TYPE_DATA | PKT_FLAG_DATA_WAPPKEY);
#else
      // just a simple data packet
      sx1272.setPacketType(PKT_TYPE_DATA);
#endif

      // Send message to the gateway and print the result
      // with the app key if this feature is enabled
#ifdef WITH_ACK
      int n_retry=NB_RETRIES;
      do {
        e = sx1272.sendPacketTimeoutACK(DEFAULT_DEST_ADDR, message, pl);

        if (e==3)
          PRINT_CSTSTR("%s","No ACK");
        n_retry--;
        if (n_retry)
          PRINT_CSTSTR("%s","Retry");
        else
          PRINT_CSTSTR("%s","Abort");

      } while (e && n_retry);
#else
      e = sx1272.sendPacketTimeout(DEFAULT_DEST_ADDR, message, pl);
#endif
      endSend = millis();

      PRINT_CSTSTR("%s","LoRa pkt size ");
      PRINT_VALUE("%d", pl);
      PRINTLN;

      PRINT_CSTSTR("%s","LoRa pkt seq ");
      PRINT_VALUE("%d", sx1272.packet_sent.packnum);
      PRINTLN;

      PRINT_CSTSTR("%s","LoRa Sent in ");
      PRINT_VALUE("%ld", endSend-startSend);
      PRINTLN;

      PRINT_CSTSTR("%s","LoRa Sent w/CAD in ");
      PRINT_VALUE("%ld", endSend-sx1272._startDoCad);
      PRINTLN;

      PRINT_CSTSTR("%s","Packet sent, state ");
      PRINT_VALUE("%d", e);
      PRINTLN;
    ESP.wdtEnable(WDTO_8S);
    count = 0;
    smartdelay(1000);
  Serial.println("Entering Sleep mode...");
  // Disable all peripherals
  digitalWrite(EN_PWR, HIGH);
  ESP.deepSleep(sleepTimeS * 1000000, WAKE_RF_DISABLED);
//  ESP.deepSleep(sleepTimeS * 1000000);

  delay(1000); // a short delay to keep in loop while Deep Sleep is being implemented. - See more at: http://www.esp8266.com/viewtopic.php?f=29&t=2472#sthash.tubcdqXf.dpuf
  }
}


/*  -------------------------------- ADXL345 --------------------------------------------------- */
void initialize_accel(){
  Serial.println("-- Initializing ADXL345 module...");

  //ADXL345
  // i2c bus SDA = GPIO0; SCL = GPIO2
//  Wire.begin();

  // Put the ADXL345 into +/- 2G range by writing the value 0x01 to the DATA_FORMAT register.
  // FYI: 0x00 = 2G, 0x01 = 4G, 0x02 = 8G, 0x03 = 16G
  accel_write(DATA_FORMAT, 0x00);

  // Put the ADXL345 into Measurement Mode by writing 0x08 to the POWER_CTL register.
  accel_write(POWER_CTL, 0x08);

  // Get 10 values and calculate calibration values.
  uint8_t i = 0;
  uint8_t j = 0;
  for(i = 0; i < 11; i++)
  {
    readAccel(false);
    for (j = 0; j < 3; j++){
    adxl_cal[j] += adxl_value[j];
  }
    delay(100);
  }

  for (j = 0; j < 3; j++){
    adxl_cal[j] /= 10;
  }

  Serial.print("-- Calibration values of ADXL345 ->  X: ");Serial.print(adxl_cal[0]);
  Serial.print("  Y: ");Serial.print(adxl_cal[1]);
  Serial.print("  Z: ");Serial.println(adxl_cal[2]);

}

// Read X, Y, Z values and store to "adxl_value" global variable...
void readAccel(bool cal)
{
//  Serial.print("readAccel");
  uint8_t howManyBytesToRead = 6; //6 for all axes
  accel_read(DATAX0, howManyBytesToRead, adxl_buff); //read the acceleration data from the ADXL345
  short x = 0;
  x = (((short)adxl_buff[1]) << 8) | adxl_buff[0];
  adxl_value[0] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;
  x = (((short)adxl_buff[3]) << 8) | adxl_buff[2];
  adxl_value[1] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;
  x = (((short)adxl_buff[5]) << 8) | adxl_buff[4];
  adxl_value[2] = x * ADXL345_MG2G_MULTIPLIER * SENSORS_GRAVITY_STANDARD;

  if (cal == true)
    for (int j = 0; j < 3; j++){
      adxl_value[j] -= adxl_cal[j];
    }
}

void accel_write(byte address, byte val)
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // send register address
  Wire.write(val); // send value to write
  Wire.endTransmission(); // end transmission
}

// Reads num bytes starting from address register on device in to _buff array
void accel_read(byte address, int num, byte _buff[])
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // sends address to read from
  Wire.endTransmission(); // end transmission
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.requestFrom(DEVICE, num); // request 6 bytes from device

  int i = 0;
  while(Wire.available()) // device may send less than requested (abnormal)
  {
    _buff[i] = Wire.read(); // receive a byte
    i++;
  }
  Wire.endTransmission(); // end transmission
}

/* ---------------------------------------- MLX96014 -------------------------------- */
//void read_mlx96014(){
//  // Read and print out the temperature
////  Serial.print("Ambient = "); Serial.print(mlx.readAmbientTempC());
////  Serial.print("*C\tObject = "); Serial.print(mlx.readObjectTempC()); Serial.println("*C");
//  temp_ambient = mlx.readAmbientTempF();
//  temp_object = mlx.readObjectTempF();
//  while (temp_ambient > 500){
//    temp_ambient = mlx.readAmbientTempF();
//    temp_object = mlx.readObjectTempF();
//  }
//}

/* ---------------------------------------- GPS -------------------------------- */

void print_gps_data(){
  gps.f_get_position(&flat, &flon, &age);
  Serial.print("LAT: "); Serial.println(flat, 6);
  Serial.print("LON: "); Serial.println(flon, 6);
}

static void smartdelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

void read_temperature_object(){
  // Read temperature from MLX96014
  int attempt = 0;
  float temp;
  while (true){
    uint16_t ret;
    Wire.beginTransmission(0x5A); // start transmission to device 
    Wire.write(0x07); // sends register address to read from
    Wire.endTransmission(false); // end transmission
    
    Wire.requestFrom((uint8_t)0x5A, (uint8_t)3);// send data n-bytes read
    ret = Wire.read(); // receive DATA
    ret |= Wire.read() << 8; // receive DATA
    uint8_t pec = Wire.read();
    
    temp = ret;
    temp *= .02;
    temp  -= 273.15;
    temp = (temp* 9 / 5) + 32;  
    if (temp < 1000)
      break;
    else if (attempt > 5){
      temp = 0;
      break;
    }
    else{
      delay(10);
      attempt ++;
    }
  }
  
  temp_object = temp;
}

void read_temperature_ambient(){
  // Read temperature from MLX96014
  int attempt = 0;
  float temp;
  while (true){
    uint16_t ret;
    Wire.beginTransmission(0x5A); // start transmission to device 
    Wire.write(0x06); // sends register address to read from
    Wire.endTransmission(false); // end transmission
    
    Wire.requestFrom((uint8_t)0x5A, (uint8_t)3);// send data n-bytes read
    ret = Wire.read(); // receive DATA
    ret |= Wire.read() << 8; // receive DATA
    uint8_t pec = Wire.read();
    
    temp = ret;
    temp *= .02;
    temp  -= 273.15;
    temp = (temp* 9 / 5) + 32;  
    if (temp < 1000)
      break;
    else if (attempt > 5){
      temp = 0;
      break;
    }
    else{
      delay(10);
      attempt ++;
    }
  }
  
  temp_ambient = temp;
}
