# Biometric Cattle Tag Project

## Components

- NodeMCU
    
    https://www.amazon.com/NodeMcu-Internet-Things-Development-ESP8266/dp/B01I9FR528/ref=sr_1_6?ie=UTF8&qid=1476989760&sr=8-6&keywords=nodemcu
    
    We will use **ESP-12E** directly in production.
    
    https://www.amazon.com/HiLetgo-ESP8266-ESP-12E-Transceiver-Wireless/dp/B010N1S5WK/ref=sr_1_3?ie=UTF8&qid=1487459354&sr=8-3&keywords=esp12e
    
- GPS module (SkyLab's SKM56A)

    The main reason why we have selected this module is because of the power consumption and its size, but need to test!
    
    http://www.skylabmodule.com/small-gpsmodule-skm56/
    
    **NOTE:** It takes 23 sec to acquire GPS data from the satellite when cold start, and 2~3 sec when warm start.
    
    Need to test both of 2 modes to evaluate power consumption.
    
- Accelerometer (ADXL345)
    
    https://www.amazon.com/Doradus-ADXL345-Digital-Acceleration-Transmission/dp/B01K83BD54/ref=sr_1_1?ie=UTF8&qid=1476995015&sr=8-1&keywords=adxl345
    
    I2C address: 0x53
    
- Pulse Sensor
    
  *MAXREFDES117* is unstable:
    
    https://www.maximintegrated.com/en/design/reference-design-center/system-board/6300.html/tb_tab0
    
  Instead of it, we will use analog heart rate sensor:
    
    https://www.sparkfun.com/products/11574
    

- Temp Sensor (MLX96014)
    
    https://www.amazon.com/Parts-tower-MLX90614ESF-Contactless-Temperature/dp/B01FWFK5TI/ref=sr_1_1?ie=UTF8&qid=1479120867&sr=8-1&keywords=MLX90614
    
    I2C address: 0x5F
    
- Lora Controller (Modtronix's InAir9)
    
    http://modtronix.com/inair9.html
    
## Wiring components.
    
### Connecting with MLX96014
    
- Pin Connection    
    
    | **NodeMCU** | **MLX90614** |
    | :----:      |   :----:     |
    |  GND        |    GND       | 
    |  3.3V       |    VDD       |
    |  D2 (GPIO4) |    SDA       |
    |  D1 (GPIO5) |    SCL       |
    
- Library:
    
    https://github.com/adafruit/Adafruit-MLX90614-Library


### Connecting with ADXL345

- Pin Connection
    
    | **NodeMCU** | **ADXL345**|
    | :----:      |   :----:   |
    |  GND        |    GND     | 
    |  3.3V       |    VCC     |
    |  D2 (GPIO4) |    SDA     |
    |  D1 (GPIO5) |    SCL     |
    |  GND        |    SDO     |
    |  3.3V       |    CS      |
     
- Library:
    
    https://github.com/sparkfun/SparkFun_ADXL345_Arduino_Library
    
    
### Connecting with MAXREFDES117 (deprecated)
    
- Pin Connection
    
    | **NodeMCU**  |   **Max**  |
    | :----:       |   :----:   |
    |  GND         |    GND     | 
    |  3.3V        |    VCC     |
    |  D2 (GPIO4)  |    SDA     |
    |  D1 (GPIO5)  |    SCL     |
    |  SD2 (GPIO9) |    INT     |
    

- Source Code:
    
    https://www.maximintegrated.com/en/design/tools/appnotes/6300/RD117_ARDUINO_V01_00.zip
    
    **NOTE**: This source code is using `Pin10` of Arduino for `INT` pin.
    Since `Pin10` is used for LoRa module, I changed it to `Pin9`.
    So, I have to modify the source code above before testing.
    
### Connecting with Analog Heart Rate Sensor

  | **NodeMCU**  |   **HR**    |
  | :----:       |   :----:    |
  |  GND         |    GND      | 
  |  3.3V        |    VCC      |
  |  A0 (ADC)    |    DOUT (S) |

### Connecting with GPS module.

**NOTE**: External 3.3V power is needed for GPS module.

- Pin Connection
    
    | **NodeMCU**  |   **GPS**  |
    | :----:       |   :----:   |
    |  GND         |    GND     | 
    |  3.3V        |    VCC     |
    |  D4 (GPIO2)  |    TX      |
    |  SD3(GPIO10) |    RX      |
     
- Library:
    
    https://github.com/florind/TinyGPS
    
### Connecting with LoRa module

- Pin Connection
    
    | **NodeMCU** |  **LoRa**  |
    | :----:      |   :----:   |
    |  GND        |    GND     | 
    |  3.3V       |    VCC     | 
    |  D5(GPIO14) |    CLK     |
    |  D6(GPIO12) |    MISO    |
    |  D7(GPIO13) |    MOSI    |
    |  D8(GPIO15) |    CS      |
    
    **NOTE 1**  Since there is no fritzing library for LoRa module, I used RFM22 module for schematic view.
    
    It has similar pinouts with LoRa.
    
    After downloading and installing `SX1272` Arduino Library, we need to change a constant value in `SX1272.h`
    
    Open `SX1272.h` in the Arduino Libraries directory, and go to line 61, and change `#define SX1272_SS 2` to `#define SX1272_SS 15`

- Connecting LoRa with RPi
     
    |  **RPi**    |  **LoRa**  |
    | :----:      |   :----:   |
    | GND         |    GND     | 
    | 3.3V        |    VCC     |
    | SCLK(GPIO11)|    CLK     |
    | MISO(GPIO9) |    MISO    |
    | MOSI(GPIO10)|    MOSI    |
    | CE_0(GPIO8) |    CS      |
    
    ![Connecting LoRa with RPi](schematic/rpi-lora.png)
     
- Library:
    

### Uploading sketch to NodeMCU.

**NOTE:** It is highly recommended to install Arduino 1.8.1 instead of the latest version.

Otherwise, uploaded sketches will not work correctly.

#### Step by step of programming.

- After installing **Arduino IDE** on your PC, goto `Tools->Board` and select `Generic ESP8266 Module`.

- Power the tag off, and remove `normal` jumper and connect `flash` jumper.

- Power the tag on. (Please make sure the `normal` jumper is not connected when booting...)

- Program on the Arduino IDE, and remove `flash` jumper and connect `normal` jumper.
    
    If `normal` jumper is not connected, tag will not wake up after turning into sleep mode.
    

## Building LoRa Gateway on RPi3

- Clone repository and build binary.

        cd ~
        git clone https://bitbucket.org/rpi_guru/pen_rider.git
        cd pen_rider/gateway
        make lora_gateway_pi2
     
- Start gateway service
    
        sudo ./lora_gateway | python lora_listener.py
        

    

    
      
    
    
    
    

    