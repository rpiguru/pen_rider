# !/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import traceback
from ubidots import ApiClient
import logging.config

try:
    is_rpi = True if os.uname()[4][:3] == 'arm' else False
except AttributeError:
    is_rpi = False

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

try:
    logging.config.fileConfig(cur_dir + "logging.conf")
except Exception as e:
    print 'Failed to load configuration of logging: {}'.format(e)

logger = logging.getLogger("ubidot")

param_names = {'ID': 'ID', 'X': 'ACC_x', 'Y': 'ACC_y', 'Z': 'ACC_z', 'T': 'Temperature', 'H_A': 'Analog Heart Rate',
               'TO': 'Object Temperature', 'TA': 'Ambient Temperature', 'H': 'Heart Rate',
               'S': 'SPO2', 'LA': 'Latitude', 'LO': 'Longitude'}

param_units = {'ACC_x': ' °', 'ACC_y': ' °', 'ACC_z': ' °', 'Object Temperature': ' °F', 'Ambient Temperature': ' °F',
               'Analog Heart Rate': ' N/sec',
               'Temperature': ' °F', 'Heart Rate': ' N/sec', 'SPO2': ' %', 'Latitude': ' °', 'Longitude': ' °'}

api = ApiClient(token='ybQF2q5BVPQ1AU2p9o79WCcUN5a0ee')


def upload(msg):
    try:
        result = {}
        cur_ds = None
        for val in str(msg).split(','):
            if val.split(':')[0] == 'ID':
                cur_ds = get_current_datasource(val.split(':')[1])
            else:
                result[param_names[val.split(':')[0]]] = float(val.split(':')[1])

        logger.debug('ID: {}'.format(cur_ds.name))
        # Pop invalid values
        for param in ['Object Temperature', 'Ambient Temperature', 'ACC_x', 'ACC_y', 'ACC_z', 'Temperature',
                      'Analog Heart Rate', 'Heart Rate', 'SPO2']:
            try:
                if result[param] == 0:
                    logger.warning(' > {} is invalid'.format(param))
                    result.pop(param, 0)
            except KeyError:
                pass
        # for param in ['Latitude', 'Longitude']:
        #     if result[param] == 1000:
        #         logger.warning(' > {} is invalid'.format(param))
        #         result.pop(param, 0)

        logger.debug(result)

        variables = cur_ds.get_variables()
        # print 'Current variables: ', variables
        collections = []
        for param in result.keys():
            if param in param_units.keys():
                # Add missing variables...
                if param not in [v.name for v in variables]:
                    try:
                        req = {"name": param, 'unit': param_units[param]}
                        logger.info('Missing variable, adding... {}'.format(req))
                        cur_ds.create_variable(req)
                        # Update variable list
                        variables = cur_ds.get_variables()
                    except ValueError:
                        logger.error('Failed to create a variable: {}'.format(param))
                        continue
                for var in variables:
                    if var.name == param:
                        collections.append({'variable': var.id, 'value': result[param]})

        # print 'Collections: ', collections
        api.save_collection(collections)
    except Exception as e:
        traceback.print_exc()
        logger.exception(e)


def get_current_datasource(_id):
    """
    Check datasource on Ubidots with given id and create new if not exists.
    :param _id:
    :return: datasource object
    """
    ds_list = api.get_datasources()
    if _id not in [d.name for d in ds_list]:
        req = {'name': _id, 'description': 'Ear Tag with ID: {}'.format(_id)}
        logger.warning('Cannot find datasource, creating new one... {}'.format(req))
        _new_ds = api.create_datasource(req)
        for _var in param_names.values():
            try:
                req = {"name": _var, 'unit': param_units[_var]}
                logger.debug('Adding variable... {}'.format(req))
                _new_ds.create_variable(req)
            except KeyError:
                pass
            except ValueError:
                logger.error('Failed to create a variable: {}'.format(_var))
            except Exception as er:
                logger.exception(er)
        return _new_ds
    else:
        for ds in ds_list:
            if _id == ds.name:
                return ds


if __name__ == '__main__':

    pass
